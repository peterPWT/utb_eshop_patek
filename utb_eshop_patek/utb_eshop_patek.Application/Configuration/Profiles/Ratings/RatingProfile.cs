﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Ratings;
using utb_eshop_patek.Domain.Entities.Ratings;

namespace utb_eshop_patek.Application.Configuration.Profiles.Ratings
{
    class RatingProfile : Profile
    {
        public RatingProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<Rating, RatingViewModel>().ReverseMap();
        }
    }
}
