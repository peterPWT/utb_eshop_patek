﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels;

namespace utb_eshop_patek.Application.Admin.ViewModels.Ratings
{
    public class RatingViewModel : BaseViewModel
    {
        public string ProductName { get; set; }
        public int ProductID { get; set; }
        public string UserName { get; set; }
        public string Comment { get; set; }
        public int RatingValue { get; set; }
    }
}
