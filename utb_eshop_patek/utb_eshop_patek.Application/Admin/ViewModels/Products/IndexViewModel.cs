﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels;

namespace utb_eshop_patek.Application.Admin.ViewModels.Products
{
    public class IndexViewModel : BaseViewModel
    {
        public List<ProductViewModel> Products { get; set; }
    }
}
