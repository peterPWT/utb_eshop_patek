﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels;
using utb_eshop_patek.Domain.Entities.Ratings;

namespace utb_eshop_patek.Application.Admin.ViewModels.Products
{
    public class ProductViewModel : BaseViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string ImageURL { get; set; }
        public IFormFile Image { get; set; }
        public IList<Rating> ratings { get; set; }
        public int ratingRatio { get; set; }
        public bool EnableRatings { get; set; }
    }
}
