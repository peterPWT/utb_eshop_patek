﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using utb_eshop_patek.Application.Admin.Mappers.Products;
using utb_eshop_patek.Application.Admin.ViewModels.Products;
using utb_eshop_patek.Domain.Entities.Products;
using utb_eshop_patek.Domain.Services.Files;
using utb_eshop_patek.Domain.Services.Products;

namespace utb_eshop_patek.Application.Admin.ApplicationServices.Products
{
    public class ProductApplicationService : IProductApplicationService
    {
        private readonly IProductMapper _productMapper;
        private readonly IProductService _productService;
        private readonly IFilesHandler _filesHandler;

        public ProductApplicationService(
            IProductMapper productMapper,
            IProductService productService,
            IFilesHandler filesHandler)
        {
            _productMapper = productMapper;
            _productService = productService;
            _filesHandler = filesHandler;
        }

        public IndexViewModel GetIndexViewModel()
        {
            return new IndexViewModel
            {
                Products = _productMapper.GetViewModelsFromEntities(
                                               _productService.GetAll()).ToList()
            };
        }

        public ProductViewModel GetProductViewModel(int id)
        {
            var vm =  _productMapper.GetViewModelFromEntity(
                _productService.Get(e => e.ID == id));

            var summary = 0;
            if (vm.ratings.Count!=0)
            {
                foreach (var rating in vm.ratings)
                {
                    summary += rating.RatingValue;
                }
                vm.ratingRatio = summary / vm.ratings.Count();
            }
            
            return vm;
        }

        public void EnableProductRatings(int productID, bool value)
        {
            _productService.EnableProductRatings(productID,value);
        }

        public ProductViewModel Insert(ProductViewModel model)
        {
            model.ImageURL = _filesHandler.SaveImage(model.Image);
            return HandleEntity(model, _productService.Insert);
        }

        public ProductViewModel Update(ProductViewModel model)
        {
            if (model.Image != null)
                model.ImageURL = _filesHandler.SaveImage(model.Image);
            return HandleEntity(model, _productService.Update);
        }

        public ProductViewModel Delete(ProductViewModel model)
        {
            return HandleEntity(model, _productService.Delete);
        }

        private ProductViewModel HandleEntity(ProductViewModel model, Func<Product, Product> func)
        {
            var entity = _productMapper.GetEntityFromViewModel(model);
            return _productMapper.GetViewModelFromEntity(func(entity));
        }
    }
}
