﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Ratings;
using utb_eshop_patek.Domain.Entities.Ratings;

namespace utb_eshop_patek.Application.Client.Mappers.Ratings
{
    public interface IRatingMapper
    {
        RatingViewModel GetViewModelFromEntity(Rating entity);
        Rating GetEntityFromViewModel(RatingViewModel viewModel);
        IList<RatingViewModel> GetViewModelsFromEntities(IList<Rating> entities);
    }
}
