﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Ratings;
using utb_eshop_patek.Domain.Entities.Ratings;

namespace utb_eshop_patek.Application.Client.Mappers.Ratings
{
    public class RatingMapper : IRatingMapper
    {
        private readonly IMapper _mapper;

        public RatingMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Rating GetEntityFromViewModel(RatingViewModel viewModel)
        {
            return _mapper.Map<Rating>(viewModel);
        }

        public RatingViewModel GetViewModelFromEntity(Rating entity)
        {
            return _mapper.Map<RatingViewModel>(entity);
        }

        public IList<RatingViewModel> GetViewModelsFromEntities(IList<Rating> entities)
        {
            return _mapper.Map<IList<RatingViewModel>>(entities);
        }
    }
}
