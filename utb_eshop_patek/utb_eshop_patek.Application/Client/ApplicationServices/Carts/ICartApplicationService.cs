﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Carts;
using utb_eshop_patek.Domain.Entities.Carts;

namespace utb_eshop_patek.Application.Client.ApplicationServices.Carts
{
    public interface ICartApplicationService
    {
        CartItem AddToCart(int productID, int amount, string userTrackingCode);
        void RemoveFromCart(int productID, string userTrackingCode);
        IndexViewModel GetIndexViewModel(string userTrackingCode);
    }
}
