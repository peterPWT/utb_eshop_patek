﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using utb_eshop_patek.Application.Client.Mappers;
using utb_eshop_patek.Application.Client.ViewModels.Carts;
using utb_eshop_patek.Domain.Entities.Carts;
using utb_eshop_patek.Domain.Services.Carts;

namespace utb_eshop_patek.Application.Client.ApplicationServices.Carts
{
    public class CartApplicationService : ICartApplicationService
    {
        private readonly ICartService _cartService;
        private readonly ICartMapper _cartMapper;

        public CartApplicationService(
            ICartService cartService,
            ICartMapper cartMapper
            )
        {
            _cartService = cartService;
            _cartMapper = cartMapper;
        }

        public CartItem AddToCart(int productID, int amount, string userTrackingCode)
        {
            return _cartService.AddToCart(productID, amount, userTrackingCode);
        }

        public IndexViewModel GetIndexViewModel(string userTrackingCode)
        {
            var items = _cartService.GetCartItems(userTrackingCode);
            return new IndexViewModel
            {
                CartItems = _cartMapper.GetViewModelsFromEntities(items),
                Total = items.Sum(cartItem => cartItem.Product.Price * cartItem.Amount)
            };
        }

        public void RemoveFromCart(int productID, string userTrackingCode)
        {
            _cartService.RemoveFromCart(productID, userTrackingCode);
        }
    }
}
