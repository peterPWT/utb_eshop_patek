﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using utb_eshop_patek.Application.Client.Mappers.Ratings;
using utb_eshop_patek.Application.Client.ViewModels.Ratings;
using utb_eshop_patek.Domain.Services.Ratings;

namespace utb_eshop_patek.Application.Client.ApplicationServices.Ratings
{
    public class RatingApplicationService : IRatingApplicationService
    {
        private readonly IRatingService _ratingService;
        private readonly IRatingMapper _ratingMapper;

        public RatingApplicationService(IRatingService ratingService, IRatingMapper ratingMapper)
        {
            _ratingService = ratingService;
            _ratingMapper = ratingMapper;
        }
        public RatingViewModel GetEmptyViewModel(int productID, string name)
        {
            return new RatingViewModel {
                ProductID = productID,
                ProductName = name
            };
        }

        public RatingViewModel GetRatingViewModel(int id)
        {
            return _ratingMapper.GetViewModelFromEntity(
                _ratingService.Get(e => e.ID == id));
        }

        public void AddRating(int productID, string comment, int rateValue, string username)
        {
            _ratingService.Create(productID,comment,rateValue,username);
        }

        public void DeleteRating(int id, bool value)
        {
            _ratingService.Delete(id,value);
        }
    }
}
