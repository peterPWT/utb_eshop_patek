﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Ratings;
using utb_eshop_patek.Domain.Entities.Ratings;

namespace utb_eshop_patek.Application.Client.ApplicationServices.Ratings
{
    public interface IRatingApplicationService
    {
        RatingViewModel GetEmptyViewModel(int productID, string name);
        RatingViewModel GetRatingViewModel(int id);
        void AddRating(int productID, string comment, int rateValue, string username);
        void DeleteRating(int id, bool value);
    }
}
