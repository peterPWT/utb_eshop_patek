﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace utb_eshop_patek.Infrastructure.Migrations
{
    public partial class RatingFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                table: "Ratings");

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Ratings",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Ratings");

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "Ratings",
                nullable: true);
        }
    }
}
