﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace utb_eshop_patek.Infrastructure.Migrations
{
    public partial class enableRatings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EnableRatings",
                schema: "Web",
                table: "Products",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnableRatings",
                schema: "Web",
                table: "Products");
        }
    }
}
