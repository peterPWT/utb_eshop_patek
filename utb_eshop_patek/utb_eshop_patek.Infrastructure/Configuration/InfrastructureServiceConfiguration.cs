﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using utb_eshop_patek.Infrastructure.Data;
using utb_eshop_patek.Infrastructure.Identity.Roles;
using utb_eshop_patek.Infrastructure.Identity.Users;

namespace utb_eshop_patek.Infrastructure.Configuration
{
    public class InfrastructureServiceConfiguration
    {
        public static void Load(IServiceCollection services, IHostingEnvironment environment)
        {
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlServer("Server = databaze.fai.utb.cz; " +
                                    "Database=A17152_A5PWT;" +
                                    "User ID=A17152;" +
                                    "Password=svoboda123;" +
                                    "persist security info=True;" +
                                    "multipleactiveResultsets=True;");
            //options.UseInMemoryDatabase("UTB.Eshop");
        });

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;

                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                options.LoginPath = "/Admin/Security/Login";
                options.LogoutPath = "/Admin/Security/Logout";
                options.SlidingExpiration = true;
            });
        }
    }
}
