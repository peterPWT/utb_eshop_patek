﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Infrastructure.Data
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseSqlServer("Server=databaze.fai.utb.cz;" +
                "Database=A17152_A5PWT;" +
                "User ID=A17152;" +
                "Password=svoboda123;" +
                "persist security info=True;" +
                "multipleactiveResultsets=True;");
            return new DataContext(builder.Options);
        }
    }
}
