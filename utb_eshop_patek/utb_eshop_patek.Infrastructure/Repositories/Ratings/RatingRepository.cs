﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using utb_eshop_patek.Domain.Entities.Products;
using utb_eshop_patek.Domain.Entities.Ratings;
using utb_eshop_patek.Domain.Repositories.Ratings;
using utb_eshop_patek.Infrastructure.Data;

namespace utb_eshop_patek.Infrastructure.Repositories.Ratings
{
    public class RatingRepository : IRatingRepository
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<Rating> _dbSet;

        public RatingRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<Rating>();
        }

        public void Create(int productID, string comment, int rateValue, string username)
        {
            var rating = new Rating
            {
                ProductID = productID,
                Comment = comment,
                RatingValue = rateValue,
                UserName = username
            };
            /*
            var product = _dbProducts.FirstOrDefault(p => p.ID == productID);

            if(product.ratings==null)
                product.ratings = new List<Rating>();

            product.ratings.Add(rating);
            */
            _dbSet.Add(rating);
            _dataContext.SaveChanges();
        }

        public void Delete(int id, bool value)
        {
            var found = _dbSet.FirstOrDefault(f => f.ID == id);
            if (found != null)
            {
                found.IsDeleted = value;
            }
            _dataContext.SaveChanges();
        }

        public Rating Get(Func<Rating, bool> predicate)
        {
            return _dbSet.FirstOrDefault(predicate);
        }

    }
}
