﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using utb_eshop_patek.Domain.Entities.Products;
using utb_eshop_patek.Domain.Entities.Ratings;
using utb_eshop_patek.Domain.Repositories.Products;
using utb_eshop_patek.Infrastructure.Data;

namespace utb_eshop_patek.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<Product> _dbSet;
        private readonly DbSet<Rating> _dbRatings;

        public ProductRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<Product>();
            _dbRatings = _dataContext.Set<Rating>();
        }

        public void EnableProductRatings(int productID, bool value)
        {
            var product = _dbSet.FirstOrDefault(p => p.ID == productID);
            product.EnableRatings = value;
            _dataContext.SaveChanges();
        }
        public Product Add(Product entity)
        {
            _dbSet.Add(entity);
            entity.EnableRatings = true;
            _dataContext.SaveChanges();

            return entity;
        }

        public Product Get(Func<Product, bool> predicate)
        {
            Product found = _dbSet.FirstOrDefault(predicate);
            found.Ratings = GetRatings(found);
            return found;
        }

        public IList<Product> GetAll()
        {
            return _dbSet
                .Where(e => !e.IsDeleted)
                .OrderByDescending(e => e.ID)
                .ToList();
        }

        public IList<Product> GetAll(Func<Product, bool> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public IList<Rating> GetRatings(Product entity)
        {
            return _dbRatings
                .Where(e => e.ProductID == entity.ID)
                .OrderByDescending(e => e.ID)
                .ToList();
        }

        public Product Remove(Product entity)
        {
            if (!IsAttached(entity))
            {
                _dbSet.Attach(entity);
            }
            entity.IsDeleted = true;
            _dataContext.SaveChanges();

            return entity;
        }

        public Product Update(Product entity)
        {
            if (!IsAttached(entity))
            {
                _dbSet.Attach(entity);
            }
            _dataContext.Entry(entity).State = EntityState.Modified;
            _dataContext.SaveChanges();

            return entity;
        }

        private bool IsAttached(Product entity)
        {
            return _dbSet.Local.Any(e => e == entity);
        }
    }
}
