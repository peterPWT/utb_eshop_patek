﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using utb_eshop_patek.Domain.Entities.Carts;
using utb_eshop_patek.Domain.Repositories.Carts;
using utb_eshop_patek.Infrastructure.Data;

namespace utb_eshop_patek.Infrastructure.Repositories.Carts
{
    public class CartRepository : ICartRepository
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<Cart> _dbSet;

        public CartRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<Cart>();
        }

        public CartItem AddToCart(int productID, int amount, string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            var cartItem = cart.CartItems.FirstOrDefault(x => x.ProductID == productID);

            if(cartItem != null)
            {
                cartItem.Amount += amount;
            }
            else
            {
                cartItem = new CartItem
                {
                    Amount = amount,
                    ProductID = productID,
                    CartID = cart.ID
                };
                cart.CartItems.Add(cartItem);
            }
            _dataContext.SaveChanges();
            return cartItem;
        }

        public IList<CartItem> GetCartItems(string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            return cart.CartItems;
        }

        public void RemoveFromCart(int productID, string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            var cartItem = cart.CartItems.FirstOrDefault(x => x.ProductID == productID);
            if (cartItem == null)
                return;

            cart.CartItems.Remove(cartItem);
            _dataContext.Entry(cartItem).State = EntityState.Deleted;
            _dataContext.SaveChanges();
        }

        public Cart GetCurrentCart(string userTrackingCode)
        {
            var cart = _dbSet
                        .Include(c => c.CartItems)
                        .ThenInclude(ci => ci.Product)
                        .FirstOrDefault(c => c.UserTrackingCode == userTrackingCode);

            if (cart == null)
            {
                cart = new Cart
                {
                    UserTrackingCode = userTrackingCode,
                    CartItems = new List<CartItem>()
                };

                _dbSet.Add(cart);
                _dataContext.SaveChanges();
            }

            return cart;
        }
    }
}
