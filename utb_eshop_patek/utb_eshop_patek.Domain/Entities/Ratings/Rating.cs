﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Domain.Entities.Ratings
{
    public class Rating : Entity
    {
        public string UserName { get; set; }
        public int ProductID { get; set; }
        public string Comment { get; set; }
        public int RatingValue { get; set; }
    }
}
