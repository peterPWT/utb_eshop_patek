﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Orders;

namespace utb_eshop_patek.Domain.Repositories.Orders
{
    public interface IOrderRepository
    {
        void CreateOrder(int userID, string userTrackingCode);
        IList<Order> GetOrders(int userID);
        IList<OrderItem> GetOrder(int orderID);
    }
}
