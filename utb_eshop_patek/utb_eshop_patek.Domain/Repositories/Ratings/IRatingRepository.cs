﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Ratings;

namespace utb_eshop_patek.Domain.Repositories.Ratings
{
    public interface IRatingRepository
    {
        void Create(int productID, string comment, int rateValue, string username);
        void Delete(int id, bool value);
        Rating Get(Func<Rating,bool> predicate);
    }
}
