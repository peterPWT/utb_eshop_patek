﻿using System.Collections.Generic;
using utb_eshop_patek.Domain.Entities.Products;
using utb_eshop_patek.Domain.Entities.Ratings;

namespace utb_eshop_patek.Domain.Repositories.Products
{
    public interface IProductRepository : IRepository<Product>
    {
        IList<Rating> GetRatings(Product entity);
        void EnableProductRatings(int productID, bool value);
    }
}
