﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Ratings;
using utb_eshop_patek.Domain.Repositories.Ratings;

namespace utb_eshop_patek.Domain.Services.Ratings
{
    public class RatingService : IRatingService
    {
        private readonly IRatingRepository _ratingRepository;

        public RatingService(IRatingRepository ratingRepository)
        {
            _ratingRepository = ratingRepository;
        }

        public void Create(int productID, string comment, int rateValue, string username)
        {
            _ratingRepository.Create(productID,comment,rateValue,username);
        }

        public void Delete(int id, bool value)
        {
            _ratingRepository.Delete(id,value);
        }
        public Rating Get(Func<Rating, bool> predicate)
        {
            return _ratingRepository.Get(predicate);
        }

    }
}
