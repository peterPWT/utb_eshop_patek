﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using utb_eshop_patek.Domain.Entities.Products;
using utb_eshop_patek.Domain.Entities.Ratings;
using utb_eshop_patek.Domain.Repositories.Products;

namespace utb_eshop_patek.Domain.Services.Products
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public void EnableProductRatings(int productID, bool value)
        {
            _productRepository.EnableProductRatings(productID, value);
        }
        public Product Delete(Product product)
        {
            return _productRepository.Remove(product);
        }

        public Product Get(Func<Product, bool> predicate)
        {
            return _productRepository.Get(predicate);
        }

        public List<Product> GetAll()
        {
            return _productRepository.GetAll().ToList();
        }

        public Product Insert(Product product)
        {
            return _productRepository.Add(product);
        }

        public Product Update(Product product)
        {
            return _productRepository.Update(product);
        }
        public IList<Rating> GetRatings(Product product)
        {
            return _productRepository.GetRatings(product); 
        }
    }
}
