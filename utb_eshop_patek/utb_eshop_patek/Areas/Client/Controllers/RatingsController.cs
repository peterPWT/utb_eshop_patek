﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using utb_eshop_patek.Application.Admin.ApplicationServices.Products;
using utb_eshop_patek.Application.Admin.ApplicationServices.Security;
using utb_eshop_patek.Application.Client.ApplicationServices.Ratings;
using utb_eshop_patek.Domain.Constants;

namespace utb_eshop_patek.Areas.Client.Controllers
{
    [Area("Client")]
    public class RatingsController : Controller
    {
        private readonly IRatingApplicationService _ratingApplicationService;
        private readonly ISecurityApplicationService _securityApplicationService;
        private readonly IProductApplicationService _productApplicationService;

        public RatingsController(IRatingApplicationService ratingApplicationService, 
            ISecurityApplicationService securityApplicationService, 
            IProductApplicationService productApplicationService)
        {
            _ratingApplicationService = ratingApplicationService;
            _securityApplicationService = securityApplicationService;
            _productApplicationService = productApplicationService;
        }

        public async Task<IActionResult> Index(int id, string name)
        {          
            var user = await _securityApplicationService.GetCurrentUser(User);

            if (user != null)
            {
                var vmEmpty = _ratingApplicationService.GetEmptyViewModel(id, name);
                vmEmpty.UserID = user?.Id;
                vmEmpty.Login = user?.Email;
                return View(vmEmpty);
            }

            return RedirectToAction("Detail", "Products", new { area = "Client" , id });
        }

        [HttpPost]
        public async Task AddRating(int id, string comment, int rateValue)
        {
            var user = await _securityApplicationService.GetCurrentUser(User);
            var username = user.Email;
            _ratingApplicationService.AddRating(id, comment, rateValue, username);
        }
    }
}