﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using utb_eshop_patek.Application.Admin.ApplicationServices.Products;
using utb_eshop_patek.Application.Admin.ViewModels.Products;
using utb_eshop_patek.Application.Client.ViewModels.Ratings;
using utb_eshop_patek.Application.Client.ApplicationServices.Ratings;
using utb_eshop_patek.Areas.Admin.Controllers.Common;

namespace utb_eshop_patek.Areas.Admin.Controllers
{
    public class ProductsController : AdminController
    {
        private readonly IProductApplicationService _productApplicationService;
        private readonly IRatingApplicationService _ratingApplicationService;

        public ProductsController(IProductApplicationService productApplicationService,
            IRatingApplicationService ratingApplicationService)
        {
            _productApplicationService = productApplicationService;
            _ratingApplicationService = ratingApplicationService;
        }

        public IActionResult Index()
        {
            var vm = _productApplicationService.GetIndexViewModel();
            return View(vm);
        }

        public IActionResult Ratings(int id)
        {
            var product = _productApplicationService.GetProductViewModel(id);
            return View(product);
        }

        public void EnableRatings(int productID,bool value)
        {
            _productApplicationService.EnableProductRatings(productID, value);
        }

        public void DeleteRating(int id,bool value)
        {
            _ratingApplicationService.DeleteRating(id,value);
        }

        public IActionResult Edit(int id)
        {
            var product = _productApplicationService.GetProductViewModel(id);
            return View(product);
        }
        
        [HttpPost]
        public IActionResult Edit(ProductViewModel model)
        {
            var product = _productApplicationService.Update(model);
            return Json(product);
        }

        public IActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Insert(ProductViewModel model)
        {
            var product = _productApplicationService.Insert(model);
            return Json(product);
        }

        public IActionResult Delete(ProductViewModel model)
        {
            _productApplicationService.Delete(model);
            return RedirectToAction(nameof(Index));
        }
    }
}